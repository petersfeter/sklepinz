﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SklepInternetowy.Core.Migrations
{
    public partial class finalSnapshot : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Active",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Gdpr",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "Location",
                table: "Orders",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Payments",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsDeleted = table.Column<bool>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    Operation_number = table.Column<string>(nullable: true),
                    Operation_type = table.Column<string>(nullable: true),
                    Operation_status = table.Column<int>(nullable: false),
                    Operation_datetime = table.Column<DateTime>(nullable: false),
                    Expiration_time = table.Column<DateTime>(nullable: false),
                    Control = table.Column<string>(maxLength: 15, nullable: true),
                    Description = table.Column<string>(maxLength: 255, nullable: true),
                    Signature = table.Column<string>(maxLength: 256, nullable: true),
                    Href = table.Column<string>(nullable: true),
                    Payment_url = table.Column<string>(nullable: true),
                    Amount = table.Column<decimal>(type: "decimal(18,2)", nullable: false),
                    Token = table.Column<string>(maxLength: 40, nullable: true),
                    Currency = table.Column<string>(maxLength: 3, nullable: true),
                    Redirection_type = table.Column<short>(nullable: false),
                    Language = table.Column<string>(maxLength: 2, nullable: true),
                    URL = table.Column<string>(nullable: true),
                    URLC = table.Column<string>(nullable: true),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Payments", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Payments");

            migrationBuilder.DropColumn(
                name: "Location",
                table: "Orders");

            migrationBuilder.AddColumn<bool>(
                name: "Active",
                table: "AspNetUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<bool>(
                name: "Gdpr",
                table: "AspNetUsers",
                type: "bit",
                nullable: false,
                defaultValue: false);
        }
    }
}
