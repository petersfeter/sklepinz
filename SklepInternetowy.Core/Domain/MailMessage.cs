﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class MailMessage : IDomainEntity
    {
        //Podstawowe propy
        [Required]
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        //---------------------------
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Content { get; set; }
        public string SentTo { get; set; }
    }
}
