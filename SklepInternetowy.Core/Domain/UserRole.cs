﻿using Microsoft.AspNetCore.Identity;
using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class UserRole : IdentityRole<Guid>, IDomainEntity
    {
        //podstawowe atrybuty
        [Required]
        public override Guid Id { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        //-------------------
        public override string Name { get; set; }
    }
}
