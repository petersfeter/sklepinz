﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class UserLocation : IDomainEntity
    {
        public Guid Id { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime DateCreated { get; set; }

        public string Name { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string ApartmentNo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public Guid UserId { get; set; }        
        //public ICollection<Order> Orders { get; set; }
    }
}
