﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class Config : IDomainEntity
    {
        //Podstawowe propy
        [Required]
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        //---------------------------
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
