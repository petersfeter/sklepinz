﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class Payment : IDomainEntity
    {
        //podstawowe atrybuty
        [Required]
        public Guid Id { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        //-------------------
        public string Operation_number { get; set; }
        public string Operation_type { get; set; }
        public Enums.PaymentStatus Operation_status { get; set; }
        public DateTime Operation_datetime { get; set; }
        public DateTime Expiration_time { get; set; }

        public string Control { get; set; }

        public string Description { get; set; }

        public string Signature { get; set; }

        public string Href { get; set; }
        public string Payment_url { get; set; }
        public decimal Amount { get; set; }

        public string Token { get; set; }

        [DefaultValue("PLN")]
        public string Currency { get; set; }
        public short Redirection_type { get; set; }

        [DefaultValue("pl")]
        public string Language { get; set; }
        public string URL { get; set; }
        public string URLC { get; set; }
        public Guid UserId { get; set; }
    }
}
