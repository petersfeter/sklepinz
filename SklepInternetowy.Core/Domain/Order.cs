﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class Order : IDomainEntity
    {
        //podstawowe atrybuty
        [Required]
        public Guid Id { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        //-------------------
        public decimal Cost { get; set; }
        public Enums.OrderStatus Status { get; set; }
        public Guid UserId { get; set; }
        [ForeignKey("UserId")]
        public User User { get; set; }
        public string Location { get; set; }
        public ICollection<OrderProduct> Products { get; set; }
    }
}