﻿using SklepInternetowy.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.Core.Domain
{
    public class Product : IDomainEntity
    {
        //podstawowe atrybuty
        [Required]
        public Guid Id { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        //-------------------
        public string Name { get; set; }
        public string Description { get; set; }

        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Nieprawidłowa cena, maksymalnie dwa miejsca po przecinku.")]
        [Range(0, 999999999.99, ErrorMessage = "Nieprawidłowa cena, maksymalna wartośc produktu jest za wysoka.")]
        public decimal Price { get; set; }
        public Enums.ProductType Type { get; set; }
        public long AvailableUnits { get; set; }
        //public string NormalizedProduct { get; set; }
        public ICollection<OrderProduct> Orders { get; set; }
    }
}