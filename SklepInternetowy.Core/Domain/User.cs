﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SklepInternetowy.Core.Interfaces;

namespace SklepInternetowy.Core.Domain
{
    public class User : IdentityUser<Guid>, IDomainEntity
    {
        //podstawowe atrybuty
        [Required]
        public override Guid Id { get; set; }
        [Required]
        public bool IsDeleted { get; set; }
        [Required]
        public DateTime DateCreated { get; set; }
        //-------------------
        public Enums.UserType Type { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Surname { get; set; }
        public override string Email { get; set; }
        public override string PhoneNumber { get; set; }
        public override string UserName { get; set; }
        public ICollection<Order> UserOrders { get; set; }
        public ICollection<UserLocation> UserLocations { get; set; }
    }
}
