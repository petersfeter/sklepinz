﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.Core.Interfaces
{
	public interface IDomainEntity
	{
		public Guid Id { get; set; }
		public bool IsDeleted { get; set; }
		public DateTime DateCreated { get; set; }
	}
}
