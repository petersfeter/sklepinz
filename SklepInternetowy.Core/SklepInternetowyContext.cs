﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SklepInternetowy.Core.Domain;

namespace SklepInternetowy.Core
{
    public class SklepInternetowyContext : IdentityDbContext<User, UserRole, Guid>
    {
        public SklepInternetowyContext(DbContextOptions<SklepInternetowyContext> options)
            : base(options)
        {
        }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<UserLocation> UserLocations { get; set; }
        public DbSet<Config> Configs { get; set; }
        public DbSet<MailMessage> MailMessages { get; set; }
        public DbSet<Payment> Payments { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var property in builder.Model.GetEntityTypes()
                .SelectMany(t => t.GetProperties())
                .Where(p => p.ClrType == typeof(decimal)))
            {
                property.SetColumnType("decimal(18,2)");
            }
            builder.Entity<Product>().Property<bool>("IsDeleted");
            builder.Entity<Product>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false)
                .Property(p => p.Type)
                .HasConversion<int>();
            builder.Entity<Order>().Property<bool>("IsDeleted");
            builder.Entity<Order>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false)
                .Property(p => p.Status)
                .HasConversion<int>();
            builder.Entity<User>().Property<bool>("IsDeleted");
            builder.Entity<User>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false)
                .Property(p => p.Type)
                .HasConversion<int>();
            builder.Entity<UserRole>().Property<bool>("IsDeleted");
            builder.Entity<UserRole>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.Entity<Config>().Property<bool>("IsDeleted");
            builder.Entity<Config>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.Entity<MailMessage>().Property<bool>("IsDeleted");
            builder.Entity<MailMessage>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.Entity<OrderProduct>().Property<bool>("IsDeleted");
            builder.Entity<OrderProduct>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.Entity<UserLocation>().Property<bool>("IsDeleted");
            builder.Entity<UserLocation>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
            builder.Entity<Payment>().Property<bool>("IsDeleted");
            builder.Entity<Payment>().HasQueryFilter(m => EF.Property<bool>(m, "IsDeleted") == false);
        }
        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }
        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries())
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        entry.CurrentValues["DateCreated"] = DateTime.UtcNow;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
    }
}
