﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.Core.Enums
{
    public enum OrderStatus
    {
        //w trakcie tworzenia (użytkownik dodaje produkty)
        Tworzone,
        //utworzone, czeka na płatność
        Utworzone,
        //opłacone
        Opłacone,
        //wysłane
        Wysłane,
        //zrealizowane
        Zrealizowane,
        //anulowane
        Anulowane
    }
}
