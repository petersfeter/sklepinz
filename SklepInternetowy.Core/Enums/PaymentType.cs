﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.Core.Enums
{
    public enum PaymentStatus
    {
        generated,
        completed,
        rejected,
        processing,
        pending
    }
}
