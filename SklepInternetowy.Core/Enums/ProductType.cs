﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.Core.Enums
{
    public enum ProductType
    {
        //procesor
        cpu,
        //chłodzenie procesora
        cooler,
        //RAM
        ram,
        //płyta główna
        mobo,
        //dysk SSD
        ssd,
        //dysk HDD
        hdd,
        //obudowa
        pcCase,
        //zasilacz
        psu,
        //karta graficzna
        gpu,
        //wentylator,myszka,klawiatura,karta sieciowa
        additional
    }
}
