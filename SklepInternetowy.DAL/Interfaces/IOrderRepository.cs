﻿using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        IQueryable<Order> GetUserOrders(Guid userId);
        Order GetIncludesById(Guid id);
        bool ActiveProductOrders(Guid id);
        Order GetUserWorkOrder(Guid userId);
        Order GetUserWorkOrderWithoutProducts(Guid userId);
        //IQueryable<Order> GetLocationOrders(Guid id);
    }
}
