﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IResultPage<TEntity> where TEntity : class
    {
        public IQueryable<TEntity> ListEntities { get; set; }
        public int TotalCount { get; set; }
    }
}
