﻿using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IPaymentRepository : IRepository<Payment>
    {
        Payment GetByOrderId(Guid id);
    }
}
