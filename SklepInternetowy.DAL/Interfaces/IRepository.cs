﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        public IResultPage<TEntity> GetPaginatedResult(IQueryable<TEntity> query, string orderBy, int page, int pageSize, string orderMethod);
        IQueryable<TEntity> GetAll();
        TEntity GetById(Guid id);
        void Create(TEntity entity);
        void Update(TEntity entity);
        void Delete(Guid id);
        void Save();
        public IResultPage<TEntity> GetPaginatedResult(IQueryable<TEntity> query, int page = 1, int pageSize = 60);
    }
}
