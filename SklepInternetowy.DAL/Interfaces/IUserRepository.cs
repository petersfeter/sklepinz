﻿using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
    }
}
