﻿using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Interfaces
{
    public interface IUserLocationRepository : IRepository<UserLocation>
    {
        IQueryable<UserLocation> GetUserLocations(Guid userId);
    }
}
