﻿using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL
{
    public interface IUnitOfWork : IDisposable
    {
        IOrderRepository Orders { get; }
        IOrderProductRepository OrderProducts { get; }
        IProductRepository Products { get; }
        IUserRepository Users { get; }
        IUserLocationRepository UserLocations { get; }
        IMailMessageRepository MailMessages { get; }
        IConfigRepository Configs { get; }
        IPaymentRepository Payments { get; }
    }
}
