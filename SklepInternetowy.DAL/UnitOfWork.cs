﻿using SklepInternetowy.Core;
using SklepInternetowy.DAL.Interfaces;
using SklepInternetowy.DAL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private IOrderRepository _orderRepository;
        private IOrderProductRepository _orderProductRepository;
        private IProductRepository _productRepository;
        private IUserRepository _userRepository;
        private IUserLocationRepository _userLocationRepository;
        private IMailMessageRepository _mailMessageRepository;
        private IConfigRepository _configRepository;
        private IPaymentRepository _paymentRepository;

        private readonly SklepInternetowyContext _context;
        public UnitOfWork(SklepInternetowyContext context)
        {
            _context = context;
        }
        public IPaymentRepository Payments
        {
            get
            {
                if (_paymentRepository == null)
                {
                    _paymentRepository = new PaymentRepository(_context);
                }
                return _paymentRepository;
            }
        }
        public IConfigRepository Configs
        {
            get
            {
                if (_configRepository == null)
                {
                    _configRepository = new ConfigRepository(_context);
                }
                return _configRepository;
            }
        }
        public IMailMessageRepository MailMessages
        {
            get
            {
                if (_mailMessageRepository == null)
                {
                    _mailMessageRepository = new MailMessageRepository(_context);
                }
                return _mailMessageRepository;
            }
        }
        public IOrderRepository Orders
        {
            get
            {
                if (_orderRepository == null)
                {
                    _orderRepository = new OrderRepository(_context);
                }
                return _orderRepository;
            }
        }
        public IOrderProductRepository OrderProducts
        {
            get
            {
                if (_orderProductRepository == null)
                {
                    _orderProductRepository = new OrderProductRepository(_context);
                }
                return _orderProductRepository;
            }
        }
        public IProductRepository Products
        {
            get
            {
                if (_productRepository == null)
                {
                    _productRepository = new ProductRepository(_context);
                }
                return _productRepository;
            }
        }
        public IUserRepository Users
        {
            get
            {
                if (_userRepository == null)
                {
                    _userRepository = new UserRepository(_context);
                }
                return _userRepository;
            }
        }
        public IUserLocationRepository UserLocations
        {
            get
            {
                if (_userLocationRepository == null)
                {
                    _userLocationRepository = new UserLocationRepository(_context);
                }
                return _userLocationRepository;
            }
        }
        public void Dispose()
        {
            _context.Dispose();
        }
    }
}