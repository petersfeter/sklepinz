﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.DAL
{
    public class CustomException : Exception
    {
        public int Status { get; set; }
        public string Title { get; set; }
        public CustomException(string title, string message) : base(message)
        {
            Title = title;
        }
    }
}
