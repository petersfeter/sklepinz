﻿using Microsoft.EntityFrameworkCore;
using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class UserLocationRepository : Repository<UserLocation>, IUserLocationRepository
    {
        public UserLocationRepository(SklepInternetowyContext context) : base(context)
        {
        }

        public IQueryable<UserLocation> GetUserLocations(Guid userId)
        {
            return Context.UserLocations.Where(x => x.UserId == userId).AsNoTracking();
        }
    }
}
