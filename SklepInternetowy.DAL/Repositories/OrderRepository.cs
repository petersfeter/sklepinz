﻿using Microsoft.EntityFrameworkCore;
using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class OrderRepository : Repository<Order>, IOrderRepository
    {
        public OrderRepository(SklepInternetowyContext context) : base(context)
        {
        }

        public bool ActiveProductOrders(Guid id)
        {

            var tmp = Context.Orders.Include(x => x.Products).Where(x =>
            x.Status != Core.Enums.OrderStatus.Anulowane
            && x.Status != Core.Enums.OrderStatus.Zrealizowane && x.Products.Select(p => p.ProductId).Contains(id));
            return tmp.Count() > 0 ? true : false;
        }

        public Order GetIncludesById(Guid id)
        {
            return Context.Orders.Include(x => x.Products).ThenInclude(x=>x.Product).FirstOrDefault(x => x.Id == id);
        }

        public IQueryable<Order> GetUserOrders(Guid userId)
        {
            return Context.Orders.Include(x => x.Products).ThenInclude(x=>x.Product).Where(x => x.UserId == userId).AsNoTracking();
        }

        public Order GetUserWorkOrder(Guid userId)
        {
            return Context.Orders.Include(x=>x.Products).ThenInclude(z=>z.Product).AsNoTracking().FirstOrDefault(x => x.UserId == userId && x.Status == Core.Enums.OrderStatus.Tworzone);
        }

        public Order GetUserWorkOrderWithoutProducts(Guid userId)
        {
            return Context.Orders.Include(x => x.Products).AsNoTracking().FirstOrDefault(x => x.UserId == userId && x.Status == Core.Enums.OrderStatus.Tworzone);
        }
    }
}
