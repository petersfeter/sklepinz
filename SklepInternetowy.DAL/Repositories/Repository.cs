﻿using Microsoft.EntityFrameworkCore;
using SklepInternetowy.Core;
using SklepInternetowy.Core.Interfaces;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class, IDomainEntity, new()
    {
        protected readonly SklepInternetowyContext Context;
        private DbSet<TEntity> _entities;
        protected virtual Expression<Func<TEntity, bool>> _isDeletedPredicate { get { return p => !p.IsDeleted; } }
        public Repository(SklepInternetowyContext context)
        {
            Context = context;
            _entities = Context.Set<TEntity>();
        }
        public IQueryable<TEntity> GetAll()
        {
            IQueryable<TEntity> result = GetIncludes(_entities).AsNoTracking();
            return result;
        }

        public IResultPage<TEntity> GetPaginatedResult(IQueryable<TEntity> query, string orderBy = "DateCreated", int page = 1, int pageSize = 60, string orderMethod = "Asc")
        {
            string command;
            if (orderMethod.Contains("asc"))
            {
                command = "OrderBy";
            }
            else command = "OrderByDescending";

            var type = typeof(TEntity);
            var property = type.GetProperty(orderBy);
            var parameter = Expression.Parameter(type, orderBy);
            var propertyAccess = Expression.MakeMemberAccess(parameter, property);
            var orderByExpression = Expression.Lambda(propertyAccess, parameter);

            var resultExpression = Expression.Call(typeof(Queryable), command, new Type[] { type, property.PropertyType },
                                          query.Expression, Expression.Quote(orderByExpression));

            var totalCount = query.Count();
            query = query.Provider.CreateQuery<TEntity>(resultExpression);
            var paginatedResult = query.Skip((page - 1) * pageSize).Take(pageSize);

            var resultPage = new ResultPage
            {
                ListEntities = paginatedResult,
                TotalCount = totalCount
            };
            return resultPage;
        }
        public IResultPage<TEntity> GetPaginatedResult(IQueryable<TEntity> query, int page = 1, int pageSize = 60)
        {
            var totalCount = query.AsEnumerable().Count();
            var paginatedResult = query.Skip((page - 1) * pageSize).Take(pageSize);

            var resultPage = new ResultPage
            {
                ListEntities = paginatedResult,
                TotalCount = totalCount
            };
            return resultPage;
        }
        public class ResultPage : IResultPage<TEntity>
        {
            public IQueryable<TEntity> ListEntities { get; set; }
            public int TotalCount { get; set; }
        }
        protected virtual IQueryable<TEntity> GetIncludes(IQueryable<TEntity> query)
        {
            return query;
        }
        public TEntity GetById(Guid id)
        {
            TEntity result = _entities.Find(id);

            return result;
        }

        public void Create(TEntity entity)
        {
            _entities.Add(entity);
        }

        public void Update(TEntity entity)
        {
            _entities.Attach(entity);
            Context.Entry(entity).State = EntityState.Modified;
        }

        public void Delete(Guid id)
        {
            TEntity existing = _entities.Find(id);
            _entities.Remove(existing);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}