﻿using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class ProductRepository: Repository<Product>, IProductRepository
    {
        public ProductRepository(SklepInternetowyContext context) : base(context)
        {
        }
    }
}
