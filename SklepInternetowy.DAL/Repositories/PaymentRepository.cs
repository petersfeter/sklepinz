﻿using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class PaymentRepository : Repository<Payment>, IPaymentRepository
    {
        public PaymentRepository(SklepInternetowyContext context) : base(context)
        {
        }

        public Payment GetByOrderId(Guid id)
        {
            return Context.Payments.FirstOrDefault(x => x.Control == id.ToString());
        }
    }
}
