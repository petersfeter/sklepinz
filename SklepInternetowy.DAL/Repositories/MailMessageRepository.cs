﻿using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class MailMessageRepository : Repository<MailMessage>, IMailMessageRepository
    {
        public MailMessageRepository(SklepInternetowyContext context) : base(context)
        {
        }
    }
}
