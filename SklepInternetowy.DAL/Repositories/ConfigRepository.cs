﻿using SklepInternetowy.Core;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.DAL.Repositories
{
    public class ConfigRepository : Repository<Config>, IConfigRepository
    {
        public ConfigRepository(SklepInternetowyContext context) : base(context)
        {
        }
    }
}
