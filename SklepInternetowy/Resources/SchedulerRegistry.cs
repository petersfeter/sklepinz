﻿using FluentScheduler;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SklepInternetowy.Resources.Jobs;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Resources
{
    public class SchedulerRegistry : Registry
    {
		public SchedulerRegistry(IServiceProvider sp)
		{
			var configuration = new ConfigurationBuilder()
				.SetBasePath(Directory.GetCurrentDirectory())
				.AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
				.Build();

			ScheduleJob<HandleExpiredPayments>(sp, configuration, "SchedulerSettings:HandleExpiredPaymentsTime");
		}
		private void ScheduleJob<T>(IServiceProvider sp, IConfigurationRoot configuration, string timePropertyName) where T : IJob
		{
			var minutes = int.Parse(configuration[timePropertyName]);

			Schedule(() => sp.CreateScope()
					.ServiceProvider.GetRequiredService<T>())
				.ToRunEvery(minutes).Minutes();
		}
	}
}
