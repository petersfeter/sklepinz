﻿using AutoMapper;
using SklepInternetowy.Areas.Identity.Pages.Account;
using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.DTOs.Product;
using SklepInternetowy.BL.DTOs.User;
using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Resources
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            // Add as many of these lines as you need to map your objects
            CreateMap<CreateUserDTO, User>();
            CreateMap<User, CreateUserDTO>();
            CreateMap<User, ListUserDTO>();
            CreateMap<User, DeleteUserDTO>();
            CreateMap<User, EditUserDTO>();
            CreateMap<User, DetailsUserDTO>()
                .ForMember(x=>x.IsActive, opt=>opt.MapFrom(s=>s.EmailConfirmed));
            CreateMap<Order, GetListOrderDTO>();
            CreateMap<Order, GetOrderDTO>();
            CreateMap<Order, ChangeStatusDTO>();
            CreateMap<Product, ListProductDTO>();
            CreateMap<Product, DeleteProductDTO>();
            CreateMap<Product, ListProductTypeDTO>();
            CreateMap<Product, CreateOrderProductDTO>();
            CreateMap<Product, InvoiceProduct>();
            CreateMap<CreateProductDTO, Product>();
            CreateMap<EditProductDTO, Product>();
            CreateMap<Product, EditProductDTO>();
            CreateMap<Product, ViewProductDTO>();
            CreateMap<UserLocation, GetLocationDTO>();
            CreateMap<UserLocation, ListUserLocationDTO>();
            CreateMap<CreateLocationDTO, UserLocation>()
                .ForMember(x=>x.Name,opt =>opt.MapFrom(s=>s.LocationName));
            CreateMap<Product, GetOrderProductDTO>()
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.Id))
                .ForMember(x => x.Price, opt => opt.MapFrom(s => s.Price))
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.Name));
            CreateMap<OrderProduct, GetOrderProductDTO>()
                .IncludeMembers(x => x.Product)
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.Product.Name))
                .ForMember(x => x.Quantity, opt => opt.MapFrom(s => s.Quantity))
                .ForMember(x => x.Price, opt => opt.MapFrom(s => s.Product.Price * s.Quantity));
            CreateMap<OrderProduct, InvoiceProduct>()
                .IncludeMembers(x => x.Product)
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.Product.Name))
                .ForMember(x => x.Quantity, opt => opt.MapFrom(s => s.Quantity))
                .ForMember(x => x.Price, opt => opt.MapFrom(s => s.Product.Price * s.Quantity));
            CreateMap<OrderProduct, CreateOrderProductDTO>()
                .IncludeMembers(x => x.Product)
                .ForMember(x => x.Id, opt => opt.MapFrom(s => s.ProductId))
                .ForMember(x => x.Name, opt => opt.MapFrom(s => s.Product.Name))
                .ForMember(x => x.Type, opt => opt.MapFrom(s => s.Product.Type));
        }
    }
}
