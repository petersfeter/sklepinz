﻿using FluentScheduler;
using SklepInternetowy.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Resources.Jobs
{
    public class HandleExpiredPayments : IJob
    {
        private readonly IOrderService _orderService;
        public HandleExpiredPayments(IOrderService orderService)
        {
            _orderService = orderService;
        }
        public void Execute()
        {
            _orderService.HandleExpiredPayments();
        }
    }
}
