﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Logging;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.Core.Domain;

namespace SklepInternetowy.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegisterModel : PageModel
    {
        private readonly IUserService _userService;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<UserRole> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly ILogger<RegisterModel> _logger;
        private readonly IMapper _mapper;

        public RegisterModel(
            IUserService userService,
            UserManager<User> userManager,
            SignInManager<User> signInManager,
            ILogger<RegisterModel> logger,
            RoleManager<UserRole> roleManager,
            IMapper mapper)
        {
            _roleManager = roleManager;
            _userService = userService;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {
            [EmailAddress]
            [Required(ErrorMessage = "Adres e-mail jest wymagany.")]
            [StringLength(100, ErrorMessage = "Adres e-mail nie może być dłuższy niż 100 znaków.")]
            [RegularExpression("^[a-zA-Z0-9_.-]{1,}[@]{1}[a-zA-Z0-9-_]+[.]{1}[a-zA-Z0-9-.]+", ErrorMessage = "Adres e-mail może zawierać litery cyfry i znaki: '.', '-' i '_'.")]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required(ErrorMessage = "Nazwa użytkownika jest wymagana.")]
            [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwa użytkownika powinna mieć od 2 do 100 znaków.")]
            [Display(Name = "UserName")]
            public string UserName { get; set; }

            [Required(ErrorMessage = "Hasło jest wymagane.")]
            [StringLength(30, MinimumLength = 8)]
            [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\D)(?=.*[0-9]).{8,30}$", ErrorMessage = "Długość hasła od 8 do 30 znaków musi zawierać co najmniej: dużą literę (A-Z), małą literę (a-z), cyfrę (0-9) i znak specjalny (np. !@#$%^&*).")]
            [DataType(DataType.Password)]
            [Display(Name = "Password")]
            public string Password { get; set; }

            [DataType(DataType.Password)]
            [Display(Name = "Confirm password")]
            [Required(ErrorMessage = "Powtórz hasło jest wymagane.")]
            [Compare("Password", ErrorMessage = "Hasła nie są zgodne.")]
            public string ConfirmPassword { get; set; }
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }

            [Required(ErrorMessage = "Imię jest wymagane.")]
            [StringLength(30, MinimumLength = 2, ErrorMessage = "Imię powinno mieć długość od 2 do 30 znaków.")]
            [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]*", ErrorMessage = "Imię nie może zawierać cyfr i znaków specjalnych.")]
            [Display(Name = "FirstName")]
            public string Firstname { get; set; }

            [Required(ErrorMessage = "Nazwisko jest wymagane.")]
            [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwisko powinno mieć długość od 2 do 100 znaków.")]
            [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,}([- ]{0,1}[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,})*", ErrorMessage = "Nazwisko nie może zawierać cyfr i znaków specjalnych, poza znakiem '-' i spacją.")]
            [Display(Name = "Surname")]
            public string Surname { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            ReturnUrl = returnUrl;
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
        }
        /// <summary>
        /// errors uzupełniam jeżeli exceptionHandler otrzyma coś - wyłapać skąd wyleciał endpoint i z odświeżyć tą stronę
        /// w exceptionie przekazuje url skąd wychodzii i wiadomość błędu
        /// </summary>
        /// <param name="returnUrl"></param>
        /// <returns></returns>
        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl ??= Url.Content("~/");
            ExternalLogins = (await _signInManager.GetExternalAuthenticationSchemesAsync()).ToList();
            if (ModelState.IsValid)
            {
                var user = new User()
                {
                    Email = Input.Email,
                    Firstname = Input.Firstname,
                    Surname = Input.Surname,
                    Type = Core.Enums.UserType.client,
                    PhoneNumber = Input.PhoneNumber,
                    UserName = Input.UserName.ToLower()
                };
                var result = await _userManager.CreateAsync(user, Input.Password);
                if (result.Succeeded)
                {

                    await _userManager.AddToRoleAsync(user, "Client");
                    _logger.LogInformation("User created a new account with password.");
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                    var callbackUrl = Url.Page(
                        "/Account/ConfirmEmail",
                        pageHandler: null,
                        values: new { area = "Identity", userId = user.Id, code = code, returnUrl = returnUrl },
                        protocol: Request.Scheme);
                    _userService.SendEmail(user, "Rejestracja konta", $"Dziękujemy za utworzenie konta w naszym serwisie. Aby aktywować konto <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>kliknij tutaj</a>. Jeżeli masz problemy z linkiem, skopiuj link i wprowadź ręcznie w pole przeglądarki: {HtmlEncoder.Default.Encode(callbackUrl)}.");

                    if (_userManager.Options.SignIn.RequireConfirmedEmail)
                    {
                        return RedirectToPage("RegisterConfirmation", new { email = Input.Email, returnUrl = returnUrl });
                    }
                    else
                    {
                        await _signInManager.SignInAsync(user, isPersistent: false);
                        return LocalRedirect(returnUrl);
                    }
                }
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
