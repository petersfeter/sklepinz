﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.Core;
using Microsoft.AspNetCore.Http;

[assembly: HostingStartup(typeof(SklepInternetowy.Areas.Identity.IdentityHostingStartup))]
namespace SklepInternetowy.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                services.Configure<IdentityOptions>(opt =>
                {
                    opt.SignIn.RequireConfirmedEmail = true;
                });

                services.AddDbContext<SklepInternetowyContext>(options =>
                    options.UseSqlServer(
                        context.Configuration.GetConnectionString("SklepInternetowyContextConnection")));
                services.AddIdentity<User, UserRole>(options => options.SignIn.RequireConfirmedEmail = true)
                        .AddEntityFrameworkStores<SklepInternetowyContext>()
                        .AddRoleManager<RoleManager<UserRole>>()
                        .AddDefaultUI()
                        .AddDefaultTokenProviders();
                services.ConfigureApplicationCookie(options =>
                {
                    options.LoginPath = $"/Identity/Account/Login";
                    options.LogoutPath = $"/Identity/Account/Logout";
                    options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
                });
            });
        }
    }
}