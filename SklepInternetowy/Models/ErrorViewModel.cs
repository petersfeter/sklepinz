using Microsoft.AspNetCore.Mvc;
using System;

namespace SklepInternetowy.Models
{
    [IgnoreAntiforgeryToken]
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public string ExceptionMessage { get; set; }
        public string Title { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
        public string ReturnUrl { get; set; }
    }
}
