﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SklepInternetowy.BL.DTOs.Product;
using SklepInternetowy.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Controllers
{
    public class ProductController : Controller
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Index(int? type, string search, string currentFilter, int? pageNumber, string sortOrder = "DateCreated", int pageSize = 15)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["TypeSortParam"] = sortOrder == "Type" ? "type_desc" : "Type";
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "dateCreated_desc" : "DateCreated";
            ViewData["UnitsSortParam"] = sortOrder == "AvailableUnits" ? "AvailableUnits_desc" : "AvailableUnits";
            ViewData["NameSortParam"] = sortOrder == "Name" ? "name_desc" : "Name";

            if (search != null)
            {
                pageNumber = 1;
            }
            else
            {
                search = currentFilter;
            }
            string orderMethod = "Asc";

            switch (sortOrder)
            {
                case "type_desc":
                    orderMethod = "Desc";
                    sortOrder = "Type";
                    break;
                case "dateCreated_desc":
                    orderMethod = "Desc";
                    sortOrder = "DateCreated";
                    break;
                case "AvailableUnits_desc":
                    orderMethod = "Desc";
                    sortOrder = "AvailableUnits";
                    break;
                case "name_desc":
                    orderMethod = "Desc";
                    sortOrder = "Name";
                    break;
                default:
                    break;
            }
            pageNumber = pageNumber ?? 1;
            ViewData["CurrentFilter"] = search;

            var result = _productService.GetAllProducts(pageNumber.Value, pageSize, sortOrder, orderMethod, type, search);

            return View(result);
        }
        [Authorize]
        public ActionResult Type(int type, string search, string currentFilter, int? pageNumber, string sortOrder = "DateCreated", int pageSize = 15)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["PriceSortParam"] = sortOrder == "Price" ? "price_desc" : "Price";
            ViewData["NameSortParam"] = sortOrder == "Name" ? "name_desc" : "Name";

            if (search != null)
            {
                pageNumber = 1;
            }
            else
            {
                search = currentFilter;
            }
            string orderMethod = "Asc";

            switch (sortOrder)
            {
                case "price_desc":
                    orderMethod = "Desc";
                    sortOrder = "Price";
                    break;
                case "name_desc":
                    orderMethod = "Desc";
                    sortOrder = "Name";
                    break;
                default:
                    break;
            }
            pageNumber = pageNumber ?? 1;
            ViewData["CurrentFilter"] = search;

            SklepInternetowy.BL.Models.PaginatedList<SklepInternetowy.BL.DTOs.Product.ListProductTypeDTO> result = _productService.GetTypeProducts(pageNumber.Value, pageSize, sortOrder, orderMethod, type, search);

            return View(result);
        }

        public IActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }
            ViewProductDTO item = _productService.GetById(id.Value);

            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        public IActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Name,Price,AvailableUnits,Type,Description")] CreateProductDTO item)
        {
            if (ModelState.IsValid)
            {
                Guid id = _productService.CreateProduct(item);
                return RedirectToAction(nameof(Details), new { id = id });
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        public IActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EditProductDTO item = _productService.GetEditProduct(id.Value);
            if (item == null)
            {
                return NotFound();
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind("Id,Name,Price,AvailableUnits,Type,Description")] EditProductDTO item)
        {
            if (ModelState.IsValid)
            {
                _productService.UpdateProduct(item);

                return RedirectToAction(nameof(Details), new { id = item.Id });
            }
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        public IActionResult Delete(Guid id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var item = _productService.GetDeleteProductDTO(id);

            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(Guid id)
        {
            _productService.DeleteProduct(id);
            return RedirectToAction(nameof(Index));
        }
    }
}
