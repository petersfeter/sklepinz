﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace SklepInternetowy.Controllers
{
    [Authorize]
    public class OrderController : Controller
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult All(string search, string currentFilter, int? pageNumber, string sortOrder = "DateCreated", int pageSize = 15)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["StatusSortParam"] = sortOrder == "Status" ? "status_desc" : "Status";
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "dateCreated_desc" : "DateCreated";
            ViewData["CostSortParam"] = sortOrder == "Cost" ? "cost_desc" : "Cost";

            if (search != null)
            {
                pageNumber = 1;
            }
            else
            {
                search = currentFilter;
            }
            string orderMethod = "Asc";

            switch (sortOrder)
            {
                case "status_desc":
                    orderMethod = "Desc";
                    sortOrder = "Status";
                    break;
                case "dateCreated_desc":
                    orderMethod = "Desc";
                    sortOrder = "DateCreated";
                    break;
                case "cost_desc":
                    orderMethod = "Desc";
                    sortOrder = "Cost";
                    break;
                default:
                    break;
            }
            pageNumber = pageNumber ?? 1;
            ViewData["CurrentFilter"] = search;
            var result = _orderService.GetAllOrders(pageNumber.Value, pageSize, sortOrder, orderMethod, search);
            return View(result);
        }
        public ActionResult My()
        {
            List<GetListOrderDTO> result = _orderService.GetCurrentUserOrders();
            
            return View(result);
        }
        public ActionResult Details(Guid id)
        {
            GetOrderDTO item = _orderService.GetOrderDTOById(id);
            return View(item);
        }
        
        public ActionResult Invoice(Guid id)
        {
            FileDTO item = _orderService.GetOrderInvoice(id);
            return File(item.File,"application/pdf",item.Name);
        }

        [Authorize(Roles = "Admin,Manager")]
        public IActionResult Edit(Guid id)
        {
            ChangeStatusDTO item = _orderService.GetChangeOrderStatus(id);
            return View(item);
        }

        [Authorize(Roles = "Admin,Manager")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit([Bind] ChangeStatusDTO dto)
        {
            if (ModelState.IsValid)
            {
                _orderService.ChangeOrderStatus(dto);
                return RedirectToAction("All");
            }
            return View(dto);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind] CreateOrderDTO createDTO)
        {
            if (ModelState.IsValid)
            {
                string result = await _orderService.CreateOrder(createDTO);
                return Redirect(result);
            }
            return View(createDTO);
        }

        public IActionResult PCMaker()
        {
            CreateOrderDTO dto = _orderService.GetWorkingOrder();
            if (dto != null)
            {
                ViewBag.locationSelect = new Microsoft.AspNetCore.Mvc.Rendering.SelectList(dto.CurrentUserLocations, "Value", "Name");
                var sum = 0.00M;
                foreach (var item in dto.Products)
                    sum += item.Price * item.Quantity;
                ViewBag.summary = sum;
            }
            else
                ViewBag.summary = 0;
            return View(dto);
        }

        public IActionResult AddOrderProduct(Guid id)
        {
            CreateOrderProductDTO item = _orderService.GetCreateOrderProductDTO(id);
            return View(item);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteOrderItem(Guid id)
        {
            _orderService.RemoveOrderItem(id);
            return RedirectToAction("PCMaker");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult AddOrderProductConfirmed(Guid id)
        {
            _orderService.AddOrderProduct(id);
            return RedirectToAction("PCMaker");
        }

        //PŁATNOŚCI
        [AllowAnonymous]
        [HttpPost]
        public HttpResponseMessage StatusReceiver([FromBody] PaymentUpdateDTO item)
        {
            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString();
            HttpStatusCode result = _orderService.UpdatePaymentStatus(item, ipAddress);
            if (result == HttpStatusCode.OK)
            {
                var resp = new HttpResponseMessage(HttpStatusCode.OK)
                {
                    Content = new StringContent("OK", System.Text.Encoding.UTF8, "text/plain")
                };
                return resp;
            }
            else return new HttpResponseMessage(result);
        }
    }
}
