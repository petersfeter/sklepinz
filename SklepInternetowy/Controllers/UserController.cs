﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.DTOs.User;
using SklepInternetowy.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Controllers
{
    [Authorize]
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        public UserController(IUserService userService)
        {
            _userService = userService;
        }
        public ActionResult Edit()
        {
            EditUserDTO dto = _userService.GetCurrentUser();
            return View(dto);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind] EditUserDTO dto)
        {
            if (ModelState.IsValid)
            {
                Guid id = _userService.EditCurrentUser(dto);
                return RedirectToAction(nameof(Details), new { id = id });
            }
            return View(dto);
        }
        #region endpointy klienta

        #region lokalizacje użytkownika
        [Authorize(Roles = "Admin,Client")]
        public ActionResult Locations()
        {
            List<ListUserLocationDTO> result = _userService.GetUserLocations();
            return View(result);
        }
        [Authorize(Roles = "Admin,Client")]
        public ActionResult LocationDetails(Guid id)
        {
            GetLocationDTO item = _userService.GetLocationDetails(id);
            return View(item);
        }
        [Authorize(Roles = "Admin,Client")]
        public ActionResult CreateLocation()
        {
            return View();
        }
        [Authorize(Roles = "Admin,Client")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateLocation([Bind] CreateLocationDTO createDTO)
        {
            if (ModelState.IsValid)
            {
                _userService.AddUserLocation(createDTO);
                return RedirectToAction(nameof(Locations));
            }
            return View(createDTO);
        }
        [Authorize(Roles = "Admin,Client")]
        public ActionResult DeleteLocation(Guid id)
        {
            string locationName = _userService.DeleteUserLocation(id);
            return View(locationName);
        }
        [Authorize(Roles = "Admin,Client")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteLocationConfirmed(Guid id)
        {
            _userService.DeleteUserLocation(id);
            return RedirectToAction(nameof(Locations));
        }

        #endregion
        #endregion


        #region zarządzanie użytkownikami - CRUD
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Index(string search, string currentFilter, int? pageNumber, string sortOrder = "DateCreated", int pageSize = 15)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["DateCreatedSortParm"] = sortOrder == "DateCreated" ? "dateCreated_desc" : "DateCreated";
            ViewData["Email"] = sortOrder == "Email" ? "email_desc" : "Email";
            ViewData["UserName"] = sortOrder == "UserName" ? "userName_desc" : "UserName";

            if (search != null)
            {
                pageNumber = 1;
            }
            else
            {
                search = currentFilter;
            }
            string orderMethod = "Asc";

            switch (sortOrder)
            {
                case "type_desc":
                    orderMethod = "Desc";
                    sortOrder = "Type";
                    break;
                case "dateCreated_desc":
                    orderMethod = "Desc";
                    sortOrder = "DateCreated";
                    break;
                case "email_desc":
                    orderMethod = "Desc";
                    sortOrder = "Email";
                    break;
                case "userName_desc":
                    orderMethod = "Desc";
                    sortOrder = "UserName";
                    break;
                default:
                    break;
            }
            pageNumber = pageNumber ?? 1;
            ViewData["CurrentFilter"] = search;
            var users = _userService.GetUsers(pageNumber.Value, pageSize, sortOrder, orderMethod, search);

            return View(users);
        }


        [Authorize]
        public ActionResult Details(Guid? id)
        {                
            DetailsUserDTO result = _userService.GetUserDetails(id);
            return View(result);
        }

        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View();
        }

        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind] CreateUserDTO createDTO)
        {
            if (ModelState.IsValid)
            {
                Guid id = await _userService.CreateUser(createDTO);
                return RedirectToAction(nameof(Details), new { id = id });
            }
            return View(createDTO);
        }
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(Guid id)
        {
            DeleteUserDTO dto = _userService.GetDeleteUserDTO(id);
            return View(dto);
        }
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            _userService.DeleteUser(id);
            return RedirectToAction(nameof(Index));
        }
        #endregion

    }
}
