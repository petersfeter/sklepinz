﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SklepInternetowy.BL.DTOs.Home;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.DAL;
using SklepInternetowy.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IHomeService _homeService;
        public HomeController(ILogger<HomeController> logger, IHomeService homeService)
        {
            _homeService = homeService;
            _logger = logger;
        }
        public IActionResult Index()
        {
            var dto = _homeService.GetCounters();
            return View(dto);
        }

        [Authorize(Roles = "Admin,Manager")]
        public IActionResult Admin()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.Client, NoStore = true)]
        public IActionResult Error()
        {
            var model = new ErrorViewModel();
            model.RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            var exceptionHandlerPathFeature =
            HttpContext.Features.Get<IExceptionHandlerPathFeature>();
            if (exceptionHandlerPathFeature?.Error is CustomException)
            {
                var customException = (CustomException)exceptionHandlerPathFeature.Error;
                model.ExceptionMessage = customException.Message;
                model.Title = customException.Title;
                _logger.LogError(model.ExceptionMessage);
            }
            if (exceptionHandlerPathFeature?.Error is Exception)
            {
                model.ExceptionMessage = exceptionHandlerPathFeature?.Error.Message;
                model.Title = "Nieoczekiwany błąd!";
                _logger.LogError(model.ExceptionMessage);
            }
            return View(model);
        }
    }
}
