﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class GetLocationDTO
    {
        public Guid Id { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string ApartmentNo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string LocationName { get; set; }
    }
}
