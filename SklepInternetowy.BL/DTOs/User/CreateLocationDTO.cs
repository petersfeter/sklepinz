﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class CreateLocationDTO
    {
        [Required(ErrorMessage = "Nazwa ulicy jest wymagana.")]
        [StringLength(200, MinimumLength = 2, ErrorMessage = "Nazwa ulicy powinna zawierać od 2 do 200 znaków.")]
        [RegularExpression("^[A-zÀ-ż0-9 .-]*$", ErrorMessage = "Nazwa ulicy nie może zawierać znaków specjalnych poza znakiem '-','.' i spacją.")]
        [Display(Name = "Street")]
        public string Street { get; set; }

        [Required(ErrorMessage = "Numer budynku jest wymagany.")]
        [StringLength(5, ErrorMessage = "Numer budynku powinien zawierać do 5 znaków.")]
        [RegularExpression(@"^\d+[a-zA-Z]{0,5}", ErrorMessage = "Numer budynku nie może zawierać znaków specjalnych.")]
        [Display(Name = "StreetNo")]
        public string StreetNo { get; set; }

        [StringLength(100, MinimumLength = 1, ErrorMessage = "Numer lokalu nie może być dłuższy niż 100 znaków.")]
        [RegularExpression("^[0-9]+[a-zA-z]*", ErrorMessage = "Numer lokalu nie może zawierać znaków specjalnych.")]
        [Display(Name = "ApartmentNo")]
        public string ApartmentNo { get; set; }

        [Required(ErrorMessage = "Nazwa miejscowości jest wymagana.")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwa miejscowości powinna zawierać od 2 do 100 znaków.")]
        [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,}([- ]{0,1}[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,})*", ErrorMessage = "Nazwa miejscowości nie może zawierać cyfr i znaków specjalnych, poza znakiem '-' i spacją.")]
        [Display(Name = "City")]
        public string City { get; set; }

        [Required(ErrorMessage = "Nazwa lokalizacji jest wymagana.")]
        [StringLength(30, MinimumLength = 1, ErrorMessage = "Nazwa lokalizacji powinna zawierać od 1 do 30 znaków.")]
        public string LocationName { get; set; }

        [Required(ErrorMessage = "Kod pocztowy jest wymagany.")]
        [RegularExpression(@"^\d{2}(-\d{3})?$", ErrorMessage = "Nieprawidłowy kod pocztowy. Kod powinien mieć format: ##-###.")]
        [Display(Name = "PostCode")]
        public string PostCode { get; set; }
    }
}
