﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class ListUserDTO
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
    }
}
