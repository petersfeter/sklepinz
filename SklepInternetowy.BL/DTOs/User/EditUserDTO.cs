﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class EditUserDTO
    {
        [Required(ErrorMessage = "Imię jest wymagane.")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Imię powinno mieć długość od 2 do 30 znaków.")]
        [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]*", ErrorMessage = "Imię nie może zawierać cyfr i znaków specjalnych.")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwisko powinno mieć długość od 2 do 100 znaków.")]
        [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,}([- ]{0,1}[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,})*", ErrorMessage = "Nazwisko nie może zawierać cyfr i znaków specjalnych, poza znakiem '-' i spacją.")]
        public string Surname { get; set; }

        [RegularExpression(@"^\+?\d{0,15}", ErrorMessage = "Numer telefonu może zawierać do 15 cyfr i początkowy znak '+'")]
        public string PhoneNumber { get; set; }
    }
}
