﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class DeleteLocationDTO
    {
        public Guid Id { get; set; }
        public string LocationName { get; set; }
    }
}
