﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class CreateUserDTO
    {
        [Required(ErrorMessage = "Typ jest wymagany.")]
        [Range(0, 1,ErrorMessage = "Dopuszczalne wartości: 0-2")]
        public Core.Enums.UserType Type { get; set; }

        [Required(ErrorMessage = "Imię jest wymagane.")]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Imię powinno mieć długość od 2 do 30 znaków.")]
        [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]*", ErrorMessage = "Imię nie może zawierać cyfr i znaków specjalnych.")]
        public string Firstname { get; set; }

        [Required(ErrorMessage = "Nazwisko jest wymagane.")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwisko powinno mieć długość od 2 do 100 znaków.")]
        [RegularExpression("^[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,}([- ]{0,1}[A-Za-zążźółćęóśńĄŻŹÓŁĆĘÓŚŃ]{1,})*", ErrorMessage = "Nazwisko nie może zawierać cyfr i znaków specjalnych, poza znakiem '-' i spacją.")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Adres e-mail jest wymagany.")]
        [StringLength(100, ErrorMessage = "Adres e-mail nie może być dłuższy niż 100 znaków.")]
        [RegularExpression("^[a-zA-Z0-9_.-]{1,}[@]{1}[a-zA-Z0-9-_]+[.]{1}[a-zA-Z0-9-.]+", ErrorMessage = "Adres e-mail może zawierać litery cyfry i znaki: '.', '-' i '_'.")]
        public string Email { get; set; }

        [RegularExpression(@"^\+?\d{0,15}", ErrorMessage = "Numer telefonu może zawierać do 15 cyfr i początkowy znak '+'")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "Nazwa użytkownika jest wymagana.")]
        [StringLength(100, MinimumLength = 2, ErrorMessage = "Nazwa użytkownika powinna mieć od 2 do 100 znaków.")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Hasło jest wymagane.")]
        [StringLength(30, MinimumLength = 8)]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\D)(?=.*[0-9]).{8,30}$", ErrorMessage = "Długość hasła od 8 do 30 znaków musi zawierać co najmniej: dużą literę (A-Z), małą literę (a-z), cyfrę (0-9) i znak specjalny (np. !@#$%^&*).")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
