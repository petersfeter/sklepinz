﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.User
{
    public class GetUserDTO
    {
        public Guid Id { get; set; }
        public Core.Enums.UserType Type { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string ApartmentNo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public bool Active { get; set; }
        //RODO
        public bool Gdpr { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string UserName { get; set; }
    }
}
