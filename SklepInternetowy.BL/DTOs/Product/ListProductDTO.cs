﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Product
{
    public class ListProductDTO
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string Name { get; set; }
        public long AvailableUnits { get; set; }
        public Core.Enums.ProductType Type { get; set; }
    }
}
