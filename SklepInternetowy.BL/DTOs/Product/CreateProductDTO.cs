﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Product
{
    public class CreateProductDTO
    {
        [Required(ErrorMessage = "Nazwa produktu jest wymagana")]
        [MaxLength(200, ErrorMessage = "Nazwa produktu nie może być dłuższa niż 200 znaków")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Cena produktu jest wymagana")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Nieprawidłowa cena, maksymalnie dwa miejsca po przecinku.")]
        [Range(0, 999999999.99, ErrorMessage = "Nieprawidłowa cena, maksymalna cena produktu jest za wysoka.")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Pole wymagane")]
        [Range(0, long.MaxValue, ErrorMessage = "Wartość nie może być ujemna.")]
        public long AvailableUnits { get; set; }

        [Required(ErrorMessage = "Typ produktu jest wymagany")]
        [Range(0, 12, ErrorMessage = "Zakres między 0-12")]
        public Core.Enums.ProductType Type { get; set; }

        [Required(ErrorMessage = "Opis produktu jest wymagany")]
        [MaxLength(1000, ErrorMessage = "Opis produktu nie może być dłuższa niż 1000 znaków")]
        public string Description { get; set; }
    }
}
