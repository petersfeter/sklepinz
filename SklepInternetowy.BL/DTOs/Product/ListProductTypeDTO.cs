﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Product
{
    public class ListProductTypeDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
    }
}
