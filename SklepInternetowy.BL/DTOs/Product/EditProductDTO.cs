﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Product
{
    public class EditProductDTO
    {
        [Required(ErrorMessage = "Identyfikator produktu jest wymagany")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Nazwa produktu jest wymagana")]
        [MaxLength(200, ErrorMessage = "Nazwa produktu nie może być dłuższa niż 200 znaków")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Cena produktu jest wymagana")]
        [RegularExpression(@"^\d+.?\d{0,2}$", ErrorMessage = "Nieprawidłowa cena, maksymalnie dwa miejsca po przecinku")]
        [Range(0, 999999999.99, ErrorMessage = "Nieprawidłowa cena, maksymalna wartośc zamówienia jest za wysoka")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Wartość produktu jest wymagana")]
        [Range(0, long.MaxValue, ErrorMessage = "Wartość nie może być ujemna.")]
        public long AvailableUnits { get; set; }

        [Required(ErrorMessage = "Typ produktu jest wymagany")]
        [Range(0, 9, ErrorMessage = "Zakres między 0-9")]
        public Core.Enums.ProductType Type { get; set; }

        [Required(ErrorMessage = "Opis produktu jest wymagana")]
        [MaxLength(1000, ErrorMessage = "Opis produktu nie może być dłuższa niż 1000 znaków")]
        public string Description { get; set; }
    }
}
