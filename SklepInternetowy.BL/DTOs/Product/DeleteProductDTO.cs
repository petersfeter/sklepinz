﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Product
{
    public class DeleteProductDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
