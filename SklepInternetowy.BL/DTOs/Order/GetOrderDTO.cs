﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class GetOrderDTO
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public decimal Cost { get; set; }
        public Core.Enums.OrderStatus Status { get; set; }
        public List<GetOrderProductDTO> OrderProducts { get; set; }
        public string Location { get; set; }
        public bool IsAdmin { get; set; }
    }
}