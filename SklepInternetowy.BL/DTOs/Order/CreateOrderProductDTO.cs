﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class CreateOrderProductDTO
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public int Quantity { get; set; }
        public Core.Enums.ProductType Type { get; set; }
    }
}
