﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class DotPayResponse
    {
        public string href { get; set; }
        public string payment_url { get; set; }
        public string token { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
        public string description { get; set; }
        public string control { get; set; }
        public string language { get; set; }
        public int redirection_type { get; set; }
        public string buttontext { get; set; }
        public string URL { get; set; }
        public string URLC { get; set; }
        public DateTime expiration_datetime { get; set; }
    }
}
