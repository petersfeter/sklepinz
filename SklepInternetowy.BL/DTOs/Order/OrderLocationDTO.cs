﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class OrderLocationDTO
    {
        public Guid Id { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public string ApartmentNo { get; set; }
        public string City { get; set; }
        public string PostCode { get; set; }
    }
}
