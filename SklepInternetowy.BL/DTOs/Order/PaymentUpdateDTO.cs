﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class PaymentUpdateDTO
    {
        public string Id { get; set; }
        public string Operation_number { get; set; }
        public string Operation_type { get; set; }
        public string Operation_status { get; set; }
        public string Operation_amount { get; set; }
        public string Operation_currency { get; set; }
        public string Operation_original_amount { get; set; }
        public string Operation_original_currency { get; set; }
        public string Operation_datetime { get; set; }
        public string Control { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string P_info { get; set; }
        public string P_email { get; set; }
        public string Channel { get; set; }
        public string Signature { get; set; }
    }
}
