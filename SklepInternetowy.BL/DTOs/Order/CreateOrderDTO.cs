﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class CreateOrderDTO
    {
        public string SelectedLocation { get; set; }
        public CreateOrderProductDTO Cpu { get; set; }
        public CreateOrderProductDTO Cooler { get; set; }
        public CreateOrderProductDTO Ram { get; set; }
        public CreateOrderProductDTO Mobo { get; set; }
        public CreateOrderProductDTO Pccase { get; set; }
        public CreateOrderProductDTO Psu { get; set; }
        public CreateOrderProductDTO Gpu { get; set; }
        public List<CreateOrderProductDTO> Disks { get; set; }
        public List<CreateOrderProductDTO> Additional { get; set; }
        public List<CreateOrderProductDTO> Products { get; set; }
        public List<LocationSelectDTO> CurrentUserLocations { get; set; }
    }
    public class LocationSelectDTO
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}
