﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class GetListOrderDTO
    {
        public Guid Id { get; set; }
        public DateTime DateCreated { get; set; }
        public Core.Enums.OrderStatus Status { get; set; }
        public decimal Cost { get; set; }
    }
}
