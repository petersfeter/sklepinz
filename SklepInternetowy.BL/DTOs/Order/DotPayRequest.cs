﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class DotPayRequest
    {
        public string api_version { get; set; }
        public string control { get; set; }
        public double amount { get; set; }
        public string description { get; set; }
        public string currency { get; set; }
        public string language { get; set; }
        public string url { get; set; }
        public string urlc { get; set; }
        public short redirection_type { get; set; }
        public string buttontext { get; set; }
        public DateTime expiration_datetime { get; set; }
        public PayerDTO payer { get; set; }
    }
}
