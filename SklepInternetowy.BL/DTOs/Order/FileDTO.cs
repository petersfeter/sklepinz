﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class FileDTO
    {
        public byte[] File { get; set; }
        public string Name { get; set; }
    }
}
