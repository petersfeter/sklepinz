﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class InvoiceModel
    {        
        public Company Company { get; set; }
        public InvoiceUser User { get; set; }
        public List<InvoiceProduct> Products { get; set; }
    }
    public class InvoiceProduct
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
    }
    public class InvoiceUser
    {
        public string Location { get; set; }
        public string Firstname { get; set; }
        public string Surname { get; set; }
        public string Phone { get; set; }
    }
    public class Company
    {
        public string Address { get; set; }
        public string Name { get; set; }
        public string NIP { get; set; }
        public string Phone { get; set; }
    }
}
