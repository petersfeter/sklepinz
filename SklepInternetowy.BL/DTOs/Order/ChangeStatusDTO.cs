﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Order
{
    public class ChangeStatusDTO
    {
        [Required(ErrorMessage = "Identyfikator zamówienia jest wymagany")]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "Wartość nie może być pusta")]
        [Range(2, 4, ErrorMessage = "Nie możesz ustawić tego statusu")]
        public int Status { get; set; }
    }
}
