﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.DTOs.Home
{
    public class HomePageDTO
    {
        public int ProductCount { get; set; }
        public int OrderCount { get; set; }
        public int AccountCount { get; set; }
    }
}
