﻿using SklepInternetowy.BL.DTOs.Product;
using SklepInternetowy.BL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.Interfaces
{
    public interface IProductService
    {
        PaginatedList<ListProductDTO> GetAllProducts(int pageNumber, int pageSize, string orderBy, string orderMethod, int? category, string search);
        ViewProductDTO GetById(Guid id);
        void UpdateProduct(EditProductDTO item);
        Guid CreateProduct(CreateProductDTO item);
        void DeleteProduct(Guid id);
        EditProductDTO GetEditProduct(Guid value);
        DeleteProductDTO GetDeleteProductDTO(Guid id);
        PaginatedList<ListProductTypeDTO> GetTypeProducts(int value, int pageSize, string sortOrder, string orderMethod, int type, string search);
    }
}
