﻿using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.Models;
using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace SklepInternetowy.BL.Interfaces
{
    public interface IOrderService
    {
        GetOrderDTO GetOrderDTOById(Guid id);
        PaginatedList<GetListOrderDTO> GetAllOrders(int value, int pageSize, string sortOrder, string orderMethod, string search);
        System.Threading.Tasks.Task<string> CreateOrder(CreateOrderDTO createDTO);
        void HandleExpiredPayments();
        void UpdateStatus(Guid id, Core.Enums.OrderStatus status);
        List<GetListOrderDTO> GetCurrentUserOrders();
        void ChangeOrderStatus(ChangeStatusDTO dto);
        ChangeStatusDTO GetChangeOrderStatus(Guid id);
        CreateOrderProductDTO GetCreateOrderProductDTO(Guid id);
        void AddOrderProduct(Guid id);
        CreateOrderDTO GetWorkingOrder();
        void RemoveOrderItem(Guid id);
        HttpStatusCode UpdatePaymentStatus(PaymentUpdateDTO item, string ipAddress);
        FileDTO GetOrderInvoice(Guid id);
    }
}
