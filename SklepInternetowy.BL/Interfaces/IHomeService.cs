﻿using SklepInternetowy.BL.DTOs.Home;
using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.Interfaces
{
    public interface IHomeService
    {
        HomePageDTO GetCounters();
    }
}
