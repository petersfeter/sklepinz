﻿using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.DTOs.User;
using SklepInternetowy.BL.Models;
using SklepInternetowy.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SklepInternetowy.BL.Interfaces
{
    public interface IUserService
    {
        void CreateUserLocation(UserLocation firstLocation);
        void AddUserLocation(CreateLocationDTO createDTO);
        PaginatedList<ListUserDTO> GetUsers(int pageNumber, int pageSize, string sortOrder, string orderMethod, string search);
        Task<Guid> CreateUser(CreateUserDTO createDTO);
        Guid EditCurrentUser(EditUserDTO dto);
        void DeleteUser(Guid id);
        EditUserDTO GetCurrentUser();
        DeleteUserDTO GetDeleteUserDTO(Guid id);
        string DeleteUserLocation(Guid id);
        List<ListUserLocationDTO> GetUserLocations();
        GetLocationDTO GetLocationDetails(Guid id);
        DetailsUserDTO GetUserDetails(Guid? id);
        void SendEmail(User user, string title, string message);
    }
}