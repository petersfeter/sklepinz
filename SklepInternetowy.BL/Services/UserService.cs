﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.DTOs.User;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.BL.Models;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SklepInternetowy.BL.Services
{
    public class UserService : Service, IUserService
    {
        private readonly UserManager<User> _userManager;
        public UserService(
            IUnitOfWork uow,
            IHttpContextAccessor httpContextAccessor,
            UserManager<User> userManager,
            IMapper mapper) : base(uow, httpContextAccessor, mapper)
        {
            _userManager = userManager;
        }

        public void AddUserLocation(CreateLocationDTO createDTO)
        {
            var userId = GetCurrentUserId();
            var domain = _mapper.Map<UserLocation>(createDTO);
            domain.UserId = userId;
            CreateUserLocation(domain);
        }

        public async Task<Guid> CreateUser(CreateUserDTO createDTO)
        {
            var user = _mapper.Map<User>(createDTO);
            user.EmailConfirmed = true;
            var result = await _userManager.CreateAsync(user, createDTO.Password);
            if (result.Succeeded)
            {

                switch (createDTO.Type)
                {
                    case Core.Enums.UserType.client:
                        await _userManager.AddToRoleAsync(user, "Client");
                        break;
                    case Core.Enums.UserType.manager:
                        await _userManager.AddToRoleAsync(user, "Manager");
                        break;
                    case Core.Enums.UserType.admin:
                        await _userManager.AddToRoleAsync(user, "Admin");
                        break;
                    default:
                        await _userManager.AddToRoleAsync(user, "Client");
                        break;
                }
                var email = $"Administrator sklepu utworzył konto \\r\\n" +
                    $"<p>Login: {createDTO.UserName} </p>\\r\\n" +
                    $"<p>Hasło: {createDTO.Password}</p> ";
                SendEmail(user, "Utworzenie konta", email);
                return user.Id;
            }
            else
            {
                var builder = new StringBuilder();
                foreach (var error in result.Errors)
                    builder.Append(error.Description);
                throw new CustomException("Błąd przy tworzeniu użytkownika", builder.ToString());
            }
        }

        public void CreateUserLocation(UserLocation firstLocation)
        {
            UOW.UserLocations.Create(firstLocation);
            UOW.UserLocations.Save();
        }

        public void DeleteUser(Guid id)
        {
            var user = UOW.Users.GetById(id);
            if (user == null)
                throw new CustomException("Błąd przy usuwaniu.", "Konto o podanych identyfikatorze nie istnieje.");
            UOW.Users.Delete(id);
            UOW.Users.Save();
        }

        public string DeleteUserLocation(Guid id)
        {
            var userId = GetCurrentUserId();
            var location = UOW.UserLocations.GetById(id);
            if (location == null)
                throw new CustomException("Błąd przy usuwaniu.", "Adres o podanym identyfikatorze nie istnieje.");
            if (location.UserId != userId)
                throw new CustomException("Błąd przy usuwaniu.", "Nie jesteś właścicielem adresu.");
            return location.Name;
        }

        public Guid EditCurrentUser(EditUserDTO dto)
        {
            var userId = GetCurrentUserId();
            var fromBase = UOW.Users.GetById(userId);
            fromBase.Firstname = dto.Firstname;
            fromBase.Surname = dto.Surname;
            fromBase.PhoneNumber = dto.PhoneNumber;
            UOW.Users.Update(fromBase);
            UOW.Users.Save();
            return userId;
        }

        public EditUserDTO GetCurrentUser()
        {
            var userId = GetCurrentUserId();
            var fromBase = UOW.Users.GetById(userId);
            return _mapper.Map<EditUserDTO>(fromBase);
        }

        public DeleteUserDTO GetDeleteUserDTO(Guid id)
        {
            var user = UOW.Users.GetById(id);
            if (user == null)
                throw new CustomException("Błąd przy usuwaniu.", "Konto o podanych identyfikatorze nie istnieje.");
            var orders = UOW.Orders.GetAll().Where(x => x.UserId == id && (x.Status == Core.Enums.OrderStatus.Opłacone || x.Status == Core.Enums.OrderStatus.Wysłane)).Any();
            if(orders)
                throw new CustomException("Błąd przy usuwaniu.", "Konto o podanych identyfikatorze posiada zamówienia będące w realizacji.");
            return _mapper.Map<DeleteUserDTO>(user);
        }

        public GetLocationDTO GetLocationDetails(Guid id)
        {
            var userId = GetCurrentUserId();
            var location = UOW.UserLocations.GetById(id);
            if (location == null)
                throw new CustomException("Błąd przy pobieraniu adresu.", "Adres o podanym identyfikatorze nie istnieje.");
            if (location.UserId != userId)
                throw new CustomException("Błąd przy pobieraniu adresu.", "Nie jesteś właścicielem adresu.");
            return _mapper.Map<GetLocationDTO>(location);
        }

        public DetailsUserDTO GetUserDetails(Guid? id)
        {
            if (!id.HasValue)
                id = GetCurrentUserId();
            var user = UOW.Users.GetById(id.Value);
            if (user == null)
                throw new CustomException("Błąd przy pobieraniu danych.", "Konto o podanych identyfikatorze nie istnieje.");
            var currentUserId = GetCurrentUserId();
            var returnObj = _mapper.Map<DetailsUserDTO>(user);
            if (id.Value == currentUserId)
            {
                returnObj.UserType = Core.Enums.UserType.client;
                return returnObj;
            }
            else
            {
                var currentUser = UOW.Users.GetById(currentUserId);
                if (currentUser.Type == Core.Enums.UserType.client)
                    throw new CustomException("Błąd przy pobieraniu danych.", "Nie posiadasz uprawnień do tej treści.");
                if (currentUser.Type == Core.Enums.UserType.admin)
                    returnObj.UserType = currentUser.Type;
                return returnObj;
            }
        }

        public List<ListUserLocationDTO> GetUserLocations()
        {
            var userId = GetCurrentUserId();
            var locations = UOW.UserLocations.GetUserLocations(userId).Select(x => _mapper.Map<ListUserLocationDTO>(x)).ToList();
            return locations;
        }

        public PaginatedList<ListUserDTO> GetUsers(int pageNumber, int pageSize, string orderBy, string orderMethod, string search = "")
        {
            var userId = GetCurrentUserId();
            var currentUser = UOW.Users.GetById(userId);

            if (search == null)
                search = "";
            IQueryable<User> query = UOW.Users.GetAll().Where(x => x.UserName.Contains(search.ToLower()) || x.Email.Contains(search.ToLower()));

            var resultPage = UOW.Users.GetPaginatedResult(query, orderBy, pageNumber, pageSize, orderMethod);

            var resultDTO = new PaginatedList<ListUserDTO>(
                resultPage.ListEntities.Select(x => _mapper.Map<ListUserDTO>(x)).ToList(),
                resultPage.TotalCount,
                pageNumber,
                pageSize);

            resultDTO.CurrentUserType = currentUser.Type;
            return resultDTO;
        }
    }
}
