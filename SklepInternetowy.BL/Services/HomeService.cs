﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using SklepInternetowy.BL.DTOs.Home;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SklepInternetowy.BL.Services
{
    public class HomeService : Service, IHomeService
    {
        public HomeService(
            IUnitOfWork uow,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper) : base(uow, httpContextAccessor, mapper)
        {
        }

        public HomePageDTO GetCounters()
        {
            var dto = new HomePageDTO();
            dto.AccountCount = UOW.Users.GetAll().Where(x => x.Type == Core.Enums.UserType.client).Count();
            dto.ProductCount = UOW.Products.GetAll().Count();
            dto.OrderCount = UOW.Orders.GetAll().Where(x => x.Status == Core.Enums.OrderStatus.Zrealizowane).Count();
            return dto;
        }
    }
}
