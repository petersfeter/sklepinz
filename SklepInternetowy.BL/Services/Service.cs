﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using MailKit.Net.Pop3;
using MailKit.Net.Smtp;
using MimeKit;
using SklepInternetowy.DAL;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Linq;
using SklepInternetowy.Core.Domain;

namespace SklepInternetowy.BL.Services
{
    public class Service
    {
        protected readonly IUnitOfWork UOW;
        protected readonly IMapper _mapper;
        protected readonly IHttpContextAccessor _httpContextAccessor;

        public Service(
            IUnitOfWork uow,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper)
        {
            this.UOW = uow;
            this._mapper = mapper;
            this._httpContextAccessor = httpContextAccessor;
        }
        public Guid GetCurrentUserId()
        {
            return Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
        }
        public string GetCurrentUserRole()
        {
            return _httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Role).ToString();
        }

        public void SendEmail(User user, string subject, string message)
        {
            var configs = UOW.Configs.GetAll().ToList();
            var mMessage = new MimeMessage();
            var senderEmail = configs.FirstOrDefault(x => x.Name == "SenderEmail").Value.ToString();
            var senderPwd = configs.FirstOrDefault(x => x.Name == "Pwd").Value.ToString();
            string senderName = configs.FirstOrDefault(x => x.Name == "SenderName").Value;
            mMessage.From.Add(new MailboxAddress(senderName, senderEmail));//list.Where(x=>x == "Sender"), list.FirstOrDefault(x => x == "Pwd")));
            mMessage.To.Add(new MailboxAddress(user.UserName, user.Email));
            mMessage.Subject = subject;
            var baseEmail = configs.FirstOrDefault(x => x.Name == "BasicEmail").Value;
            baseEmail = baseEmail.Replace("$$nazwa_uzytkownika", user.UserName);
            baseEmail = baseEmail.Replace("$$tresc_maila", message);
            mMessage.Body = new TextPart("html")
            {
                Text = baseEmail                
            };
            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(configs.FirstOrDefault(x => x.Name == "MailServer").Value.ToString(), int.Parse(configs.FirstOrDefault(x => x.Name == "MailPort").Value), bool.Parse(configs.FirstOrDefault(x => x.Name == "Ssl").Value));
                    client.Authenticate(senderEmail, senderPwd);
                    client.Send(mMessage);
                    client.Disconnect(true);
                    CreateDomainMailMessage(mMessage);
                };
            }
            catch (Exception e)
            {
                throw new CustomException("Błąd wysłania wiadomości.", e.Message);//"Podany adres e-mail nie istnieje. Zweryfikuj poprawność wprowadzonego adresu.");
            }
        }
        private void CreateDomainMailMessage(MimeMessage mMessage)
        {
            Core.Domain.MailMessage domainMail = new Core.Domain.MailMessage()
            {
                Content = mMessage.Body.ToString(),
                SentTo = mMessage.To.ToString(),
                Subject = mMessage.Subject
            };
            UOW.MailMessages.Create(domainMail);
            UOW.MailMessages.Save();
        }

        public void SendEmail(User user, string subject, string message, DTOs.Order.FileDTO invoice)
        {
            var configs = UOW.Configs.GetAll().ToList();
            var mMessage = new MimeMessage();
            var senderEmail = configs.FirstOrDefault(x => x.Name == "SenderEmail").Value.ToString();
            var senderPwd = configs.FirstOrDefault(x => x.Name == "Pwd").Value.ToString();
            string senderName = configs.FirstOrDefault(x => x.Name == "SenderName").Value;
            mMessage.From.Add(new MailboxAddress(senderName, senderEmail));//list.Where(x=>x == "Sender"), list.FirstOrDefault(x => x == "Pwd")));
            mMessage.To.Add(new MailboxAddress(user.UserName, user.Email));
            mMessage.Subject = subject;
            var baseEmail = configs.FirstOrDefault(x => x.Name == "BasicEmail").Value;
            baseEmail = baseEmail.Replace("$$nazwa_uzytkownika", user.UserName);
            baseEmail = baseEmail.Replace("$$tresc_maila", message);

            var bodyBuilder = new BodyBuilder { HtmlBody = message };
            bodyBuilder.Attachments.Add(invoice.Name, invoice.File, ContentType.Parse("application/pdf"));
            mMessage.Body = bodyBuilder.ToMessageBody();
            try
            {
                using (var client = new SmtpClient())
                {
                    client.ServerCertificateValidationCallback = (s, c, h, e) => true;
                    client.Connect(configs.FirstOrDefault(x => x.Name == "MailServer").Value.ToString(), int.Parse(configs.FirstOrDefault(x => x.Name == "MailPort").Value), bool.Parse(configs.FirstOrDefault(x => x.Name == "Ssl").Value));
                    client.Authenticate(senderEmail, senderPwd);
                    client.Send(mMessage);
                    client.Disconnect(true);
                    CreateDomainMailMessage(mMessage);
                };
            }
            catch (Exception e)
            {
                throw new CustomException("Błąd wysłania wiadomości.", e.Message);//"Podany adres e-mail nie istnieje. Zweryfikuj poprawność wprowadzonego adresu.");
            }
        }
    }
}
