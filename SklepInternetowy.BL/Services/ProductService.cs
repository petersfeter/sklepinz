﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using SklepInternetowy.BL.DTOs.Product;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.BL.Models;
using SklepInternetowy.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;

namespace SklepInternetowy.BL.Services
{
    public class ProductService : Service, IProductService
    {
        public ProductService(
            IUnitOfWork uow,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper) : base(uow, httpContextAccessor, mapper)
        {
        }

        public Guid CreateProduct(CreateProductDTO item)
        {
            //var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var domain = _mapper.Map<Product>(item);
            UOW.Products.Create(domain);
            UOW.Products.Save();
            return domain.Id;
        }

        public void DeleteProduct(Guid id)
        {
            var domain = UOW.Products.GetById(id);
            if (domain == null)
                throw new CustomException("Brak danych", "Produkt o podanym identyfikatorze nie istnieje.");
            bool hasOrders = UOW.Orders.ActiveProductOrders(id);
            if(hasOrders)
                throw new CustomException("Błąd usuwania produktu", "Produkt o podanym identyfikatorze posiada zamówienia będące w realizacji.");
            UOW.Products.Delete(id);
            UOW.Products.Save();
        }

        public PaginatedList<ListProductDTO> GetAllProducts(int pageNumber, int pageSize, string orderBy, string orderMethod, int? type, string search)
        {
            if (search == null)
                search = "";
            IQueryable<Product> query = UOW.Products.GetAll().Where(x => x.Name.Contains(search.ToLower()) || x.Description.Contains(search.ToLower()));
            if (type.HasValue)
                query = query.Where(x => x.Type == (Core.Enums.ProductType)type.Value);

            var resultPage = UOW.Products.GetPaginatedResult(query, orderBy, pageNumber, pageSize, orderMethod);
            //var resultPage = UOW.Products.GetAll();

            var resultDTO = new PaginatedList<ListProductDTO>(
                resultPage.ListEntities.Select(x => _mapper.Map<ListProductDTO>(x)).ToList(),
                resultPage.TotalCount,
                pageNumber,
                pageSize);

            var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var user = UOW.Users.GetById(userId);
            resultDTO.CurrentUserType = user.Type;

            return resultDTO;
        }

        public ViewProductDTO GetById(Guid id)
        {
            var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var user = UOW.Users.GetById(userId);
            var domain = UOW.Products.GetById(id);
            if (domain == null)
                throw new CustomException("Brak danych", "Produkt o podanym identyfikatorze nie istnieje.");

            var returnObj = _mapper.Map<ViewProductDTO>(domain);
            if (user != null && (user.Type == Core.Enums.UserType.admin || user.Type == Core.Enums.UserType.manager))
                returnObj.IsAdmin = true;
            return returnObj;
        }

        public DeleteProductDTO GetDeleteProductDTO(Guid id)
        {
            var domain = UOW.Products.GetById(id);
            if (domain == null)
                throw new CustomException("Brak danych", "Produkt o podanym identyfikatorze nie istnieje.");
            return _mapper.Map<DeleteProductDTO>(domain);
        }

        public EditProductDTO GetEditProduct(Guid id)
        {
            var domain = UOW.Products.GetById(id);
            if (domain == null)
                throw new CustomException("Brak danych", "Produkt o podanym identyfikatorze nie istnieje.");

            var returnObj = _mapper.Map<EditProductDTO>(domain);
            return returnObj;
        }

        public PaginatedList<ListProductTypeDTO> GetTypeProducts(int pageNumber, int pageSize, string orderBy, string orderMethod, int type, string search)
        {
            if (search == null)
                search = "";
            IQueryable<Product> query = UOW.Products.GetAll().Where(x =>x.AvailableUnits > 0 && (x.Name.Contains(search.ToLower()) || x.Description.Contains(search.ToLower())) && x.Type == (Core.Enums.ProductType)type);

            var resultPage = UOW.Products.GetPaginatedResult(query, orderBy, pageNumber, pageSize, orderMethod);
            //var resultPage = UOW.Products.GetAll();

            var resultDTO = new PaginatedList<ListProductTypeDTO>(
                resultPage.ListEntities.Select(x => _mapper.Map<ListProductTypeDTO>(x)).ToList(),
                resultPage.TotalCount,
                pageNumber,
                pageSize);

            var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            var user = UOW.Users.GetById(userId);
            resultDTO.CurrentUserType = user.Type;

            return resultDTO;
        }

        public void UpdateProduct(EditProductDTO item)
        {
            var domain = _mapper.Map<Product>(item);
            UOW.Products.Update(domain);
            UOW.Products.Save();
        }
    }
}
