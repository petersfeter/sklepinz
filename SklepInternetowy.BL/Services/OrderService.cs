﻿using AutoMapper;
using DinkToPdf;
using DinkToPdf.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using SklepInternetowy.BL.DTOs.Order;
using SklepInternetowy.BL.Interfaces;
using SklepInternetowy.BL.Models;
using SklepInternetowy.Core.Domain;
using SklepInternetowy.DAL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SklepInternetowy.BL.Services
{
    public class OrderService : Service, IOrderService
    {
        private HttpClient client;
        private IConverter _converter;
        public OrderService(
            IUnitOfWork uow,
            IConverter converter,
            IHttpContextAccessor httpContextAccessor,
            IMapper mapper) : base(uow, httpContextAccessor, mapper)
        {
            _converter = converter;
            client = new HttpClient();
        }

        public async Task<string> CreateOrder(CreateOrderDTO createDTO)
        {
            var userId = GetCurrentUserId();
            var user = UOW.Users.GetById(userId);
            if (user.Type == Core.Enums.UserType.admin || user.Type == Core.Enums.UserType.manager)
                throw new CustomException("Niewłaściwe konto", "Zaloguj się na konto użytkownika");
            Order order = UOW.Orders.GetUserWorkOrderWithoutProducts(userId);
            if (order == null)
                throw new CustomException("Błąd przy tworzeniu zamówienia", "Twoje zamówienie jest puste.");

            var cost = 0.00M;
            var productIds = order.Products.Select(x => x.ProductId).ToList();
            var products = UOW.Products.GetAll().Where(x => productIds.Contains(x.Id) && x.AvailableUnits > 0).AsNoTracking().ToList();
            if (products.Count() < productIds.Count)
                throw new CustomException("Błąd przy tworzeniu zamówienia", "Wybrano produkty niedostepne w magazynie.");

            foreach (var item in order.Products)
            {
                var tmp = products.First(x => x.Id == item.ProductId);
                if (tmp.AvailableUnits < item.Quantity)
                    throw new CustomException("Błąd przy tworzeniu zamówienia", "Przepraszamy, obecny stan magazynu wynosi: " + tmp.AvailableUnits.ToString());
                tmp.AvailableUnits -= item.Quantity;
                cost += 1 * tmp.Price;
                UOW.Products.Update(tmp);
                UOW.Products.Save();
            }
            order.Cost = cost;
            order.Status = Core.Enums.OrderStatus.Utworzone;
            order.Location = createDTO.SelectedLocation;
            UOW.Orders.Update(order);
            UOW.Orders.Save();
            return await GeneratePayment(order, user);
        }

        private async Task<string> GeneratePayment(Order order, User user)
        {
            var configs = UOW.Configs.GetAll().ToList();
            var uri = $"{configs.FirstOrDefault(x => x.Name == "DotPayApi").Value}{configs.FirstOrDefault(x => x.Name == "ShopId").Value}/payment_links/";
            var payment = new Payment()
            {
                Amount = order.Cost,
                Control = order.Id.ToString(),
                Description = $"Opłata za zamówienie nr {order.Id}",
                Operation_datetime = DateTime.UtcNow,
                Operation_status = Core.Enums.PaymentStatus.generated,
                UserId = user.Id,
                URLC = configs.FirstOrDefault(x => x.Name == "URLC").Value,
                URL = configs.FirstOrDefault(x => x.Name == "URL").Value,
                Redirection_type = 0,
                Currency = "PLN",
                Href = "",
                Language = "pl",
                Operation_number = "",
                Operation_type = "",
                Payment_url = "",
                Signature = "",
                Token = "",
                Expiration_time = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), "Central Europe Standard Time").AddHours(1).AddMinutes(10)
            };
            UOW.Payments.Create(payment);
            UOW.Payments.Save();
            var paymentDTO = new DotPayRequest()
            {
                amount = (double)order.Cost,//double.Parse(order.Cost.ToString("G29", System.Globalization.CultureInfo.CreateSpecificCulture("en-US"))),
                api_version = configs.FirstOrDefault(x => x.Name == "DPApiVersion").Value,
                buttontext = "Powrót",
                control = payment.Control,
                currency = payment.Currency,
                description = payment.Description,
                expiration_datetime = payment.Expiration_time,
                language = payment.Language,
                payer = new PayerDTO() { email = user.Email, first_name = user.Firstname, last_name = user.Surname },
                redirection_type = payment.Redirection_type,
                url = payment.URL,
                urlc = payment.URLC
            };
            var body = JsonSerializer.Serialize(paymentDTO);
            var response = await PostDotPay(uri, body, configs.FirstOrDefault(x => x.Name == "DPUser").Value, configs.FirstOrDefault(x => x.Name == "DPPwd").Value);
            var responseObj = JsonSerializer.Deserialize<DotPayResponse>(response);

            //generowanie parametru CHK
            var chkString = configs.FirstOrDefault(x => x.Name == "DPPIN").Value + responseObj.token + "K,T" + "1";
            SHA256 sha256 = SHA256.Create();
            byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(chkString));

            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }

            payment.Payment_url = responseObj.payment_url + "&channel_groups=K,T&ignore_last_payment_channel=1" + "&chk=" + builder.ToString();
            SendEmail(user, "Potwierdzenie złożenia zamówienia", $"Dziękujemy za złożenie zamówienia. W celu uiszczenia opłaty proszę skorzystać z <a href='{payment.Payment_url}'>linku</a>. Jeżeli masz problemy z linkiem, skopiuj link i wprowadź ręcznie w pole przeglądarki: {payment.Payment_url}. W przypadku zmiany statusu zamówienia zostaniesz o tym poinformowany w kolejnej wiadomości.");
            payment.Href = responseObj.href;
            payment.Token = responseObj.token;
            payment.Operation_status = Core.Enums.PaymentStatus.pending;
            UOW.Payments.Update(payment);
            UOW.Payments.Save();

            return payment.Payment_url;
        }

        private async Task<string> PostDotPay(string uri, string body, string dpUser, string dpPassword)
        {
            string content;
            var authToken = Encoding.ASCII.GetBytes($"{dpUser}:{dpPassword}");
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic",
                Convert.ToBase64String(authToken));
            StringContent httpContent = new StringContent(body, Encoding.UTF8, "application/json");
            System.Net.ServicePointManager.SecurityProtocol = System.Net.SecurityProtocolType.Tls13 | System.Net.SecurityProtocolType.Tls12;
            //wysłanie zapytania
            HttpResponseMessage response = await client.PostAsync(uri, httpContent);

            if (response.IsSuccessStatusCode)
            {
                content = await response.Content.ReadAsStringAsync();
                var x = response.RequestMessage.RequestUri.ToString();
            }
            else
            {
                throw new CustomException("Błąd przy generowaniu płatności", response.RequestMessage.ToString());
            }
            return content;
        }

        public void UpdateStatus(Guid id, Core.Enums.OrderStatus status)
        {
            var role = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.Role));
            if (role.ToString().ToLower() == "admin" || role.ToString().ToLower() == "manager")
            {
                var order = UOW.Orders.GetById(id);
                order.Status = status;
                UOW.Orders.Update(order);
                UOW.Orders.Save();
            }
            throw new CustomException("Brak dostępu", "Nie masz uprawnień do wykonania tej akcji");
        }

        public PaginatedList<GetListOrderDTO> GetAllOrders(int pageNumber, int pageSize, string orderBy, string orderMethod, string search)
        {
            if (search == null)
                search = "";
            IQueryable<Order> query = UOW.Orders.GetAll().Where(x => x.Id.ToString().Contains(search.ToLower()));

            var resultPage = UOW.Orders.GetPaginatedResult(query, orderBy, pageNumber, pageSize, orderMethod);
            //var resultPage = UOW.Products.GetAll();

            var resultDTO = new PaginatedList<GetListOrderDTO>(
                resultPage.ListEntities.Select(x => _mapper.Map<GetListOrderDTO>(x)).ToList(),
                resultPage.TotalCount,
                pageNumber,
                pageSize);

            return resultDTO;
        }

        public GetOrderDTO GetOrderDTOById(Guid id)
        {
            var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            Order order = UOW.Orders.GetIncludesById(id);
            var currentUser = UOW.Users.GetById(userId);
            if (currentUser.Type == Core.Enums.UserType.client && order.UserId != userId)
                throw new CustomException("Brak dostępu", "Nie masz uprawnień do wykonania tej akcji");
            var returnObj = _mapper.Map<GetOrderDTO>(order);
            //if (order.Location.ApartmentNo != "")
            //    returnObj.Location = $"{order.Location.Street} {order.Location.StreetNo}/{order.Location.ApartmentNo} {order.Location.City} {order.Location.PostCode}";//_mapper.Map<OrderLocationDTO>(order.Location);
            //else
            //    returnObj.Location = $"{order.Location.Street} {order.Location.StreetNo} {order.Location.City} {order.Location.PostCode}";//_mapper.Map<OrderLocationDTO>(order.Location);
            if (currentUser.Type != Core.Enums.UserType.client)
                returnObj.IsAdmin = true;
            returnObj.OrderProducts = order.Products.Select(x => _mapper.Map<GetOrderProductDTO>(x)).ToList();
            return returnObj;
        }

        public List<GetListOrderDTO> GetCurrentUserOrders()
        {
            var userId = GetCurrentUserId();
            var orders = UOW.Orders.GetAll().Where(x => x.UserId == userId).OrderByDescending(x => x.DateCreated).Select(x => _mapper.Map<GetListOrderDTO>(x)).ToList();
            return orders;
        }

        public void ChangeOrderStatus(ChangeStatusDTO dto)
        {
            var order = UOW.Orders.GetById(dto.Id);
            if (order == null)
                throw new CustomException("Brak danych", "Zamówienie o podanym identyfikatorze nie istnieje.");
            order.Status = (Core.Enums.OrderStatus)dto.Status;
            UOW.Orders.Update(order);
            UOW.Orders.Save();
            var orderUser = UOW.Users.GetById(order.UserId);
            if (order.Status == Core.Enums.OrderStatus.Zrealizowane)
            {
                var invoice = GetOrderInvoice(order.Id);
                SendEmail(orderUser, "Aktualizacja statusu zamówienia", $"Twoje zamówienie zostało zrealizowane. W załączniku znajduje się wygenerowana e-faktura. Dla każdego zrealizowanego zamówienia faktury dostępne są również w panelu Zamówienia->Szczegóły. Dziękujemy za zakupy w naszym sklepie.",invoice);
            }
            else
                SendEmail(orderUser, "Aktualizacja statusu zamówienia", $"Status Twojego zamówienia został zmieniony na {order.Status.ToString()}.");
        }

        public ChangeStatusDTO GetChangeOrderStatus(Guid id)
        {
            var order = UOW.Orders.GetById(id);
            if (order == null)
                throw new CustomException("Brak danych", "Zamówienie o podanym identyfikatorze nie istnieje.");
            return _mapper.Map<ChangeStatusDTO>(order);
        }

        public CreateOrderProductDTO GetCreateOrderProductDTO(Guid id)
        {
            var product = UOW.Products.GetById(id);
            if (product == null)
                throw new CustomException("Brak danych", "Produkt o podanym identyfikatorze nie istnieje.");
            return _mapper.Map<CreateOrderProductDTO>(product);
        }

        public void AddOrderProduct(Guid id)
        {
            var userId = GetCurrentUserId();
            Order userOrder = UOW.Orders.GetUserWorkOrder(userId);
            if (userOrder == null)
            {
                userOrder = new Order()
                {
                    Status = Core.Enums.OrderStatus.Tworzone,
                    UserId = userId
                };
                UOW.Orders.Create(userOrder);
                UOW.Orders.Save();
            }
            var product = UOW.Products.GetById(id);

            OrderProduct orderProduct = null;
            if (userOrder.Products != null && userOrder.Products.Count > 0)
                orderProduct = userOrder.Products.FirstOrDefault(x => x.Product.Type == product.Type);
            if (orderProduct == null)
            {
                CreateOrderProduct(userOrder.Id, id);
            }
            else
            {
                if (orderProduct.Product.Type == Core.Enums.ProductType.ssd
                    || orderProduct.Product.Type == Core.Enums.ProductType.hdd
                    || orderProduct.Product.Type == Core.Enums.ProductType.additional)
                {
                    if (orderProduct.ProductId == id)
                        orderProduct.Quantity += 1;
                    else
                    {
                        CreateOrderProduct(userOrder.Id, id);
                    }
                }
                else
                {
                    orderProduct.ProductId = id;
                }
                UOW.OrderProducts.Update(orderProduct);
                UOW.OrderProducts.Save();
            }
        }
        private void CreateOrderProduct(Guid orderId, Guid productId)
        {
            var orderProduct = new OrderProduct()
            {
                ProductId = productId,
                OrderId = orderId,
                Quantity = 1
            };
            UOW.OrderProducts.Create(orderProduct);
            UOW.OrderProducts.Save();
        }

        public CreateOrderDTO GetWorkingOrder()
        {
            var userId = GetCurrentUserId();
            Order userOrder = UOW.Orders.GetUserWorkOrder(userId);
            if (userOrder == null)
                return null;
            var userLocations = UOW.UserLocations.GetUserLocations(userId).ToList();
            var list = new List<LocationSelectDTO>();
            foreach (var item in userLocations)
            {
                if (item.ApartmentNo == null || item.ApartmentNo == "")
                    list.Add(new LocationSelectDTO()
                    {
                        Value = $"{item.Street} {item.StreetNo} {item.City} {item.PostCode}",
                        Name = item.Name
                    });
                else
                    list.Add(new LocationSelectDTO()
                    {
                        Value = $"{item.Street} {item.StreetNo}/{item.ApartmentNo} {item.City} {item.PostCode}",
                        Name = item.Name
                    });
            }
            var returnObj = new CreateOrderDTO()
            {
                Cpu = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.cpu)),
                Cooler = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.cooler)),
                Ram = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.ram)),
                Mobo = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.mobo)),
                Pccase = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.pcCase)),
                Psu = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.psu)),
                Gpu = _mapper.Map<CreateOrderProductDTO>(userOrder.Products.FirstOrDefault(x => x.Product.Type == Core.Enums.ProductType.gpu)),
                Additional = userOrder.Products.Where(x => x.Product.Type == Core.Enums.ProductType.additional).Select(x => _mapper.Map<CreateOrderProductDTO>(x)).ToList(),
                Disks = userOrder.Products.Where(x => x.Product.Type == Core.Enums.ProductType.ssd || x.Product.Type == Core.Enums.ProductType.hdd).Select(x => _mapper.Map<CreateOrderProductDTO>(x)).ToList(),
                Products = userOrder.Products.Select(x => _mapper.Map<CreateOrderProductDTO>(x)).ToList(),
                CurrentUserLocations = list
            };
            return returnObj;
        }

        public void RemoveOrderItem(Guid id)
        {
            var userId = GetCurrentUserId();
            Order userOrder = UOW.Orders.GetUserWorkOrder(userId);
            var remove = userOrder.Products.FirstOrDefault(x => x.ProductId == id);
            if (remove == null)
                throw new CustomException("Błąd danych", "Produkt został usunięty.");
            UOW.OrderProducts.Delete(remove.Id);
            UOW.OrderProducts.Save();
        }

        public HttpStatusCode UpdatePaymentStatus(PaymentUpdateDTO item, string ipAddress)
        {
            var configs = UOW.Configs.GetAll().ToList();
            string unrestrictedIp = configs.FirstOrDefault(x => x.Name == "DotPayIpAdress").Value;

            if (ipAddress == unrestrictedIp) //czy ip się zgadza
            {
                if (checkSignature(item, configs.FirstOrDefault(x => x.Name == "DPPIN").Value))   //czy sygnatura się zgadza
                {
                    Payment payment = UOW.Payments.GetByOrderId(Guid.Parse(item.Control));
                    if (payment == null)
                        return HttpStatusCode.BadRequest;
                    payment.Operation_status = Enum.Parse<Core.Enums.PaymentStatus>(item.Operation_status);
                    payment.Operation_number = item.Operation_number;
                    payment.Operation_datetime = DateTime.Parse(item.Operation_datetime);
                    payment.Operation_type = item.Operation_type;
                    UOW.Payments.Update(payment);
                    UOW.Payments.Save();
                    var order = UOW.Orders.GetById(Guid.Parse(payment.Control));
                    var user = UOW.Users.GetById(payment.UserId);
                    if (payment.Operation_status == Core.Enums.PaymentStatus.completed)
                    {
                        order.Status = Core.Enums.OrderStatus.Opłacone;
                        UOW.Orders.Update(order);
                        UOW.Orders.Save();

                        SendEmail(user, "Aktualizacja statusu zamówienia", $"Dziękujemy za dokonanie płatności. Status Twojego zamówienia został zmieniony na {order.Status.ToString()}.");
                    }
                    else
                    {
                        var status = "";
                        switch (payment.Operation_status)
                        {
                            case Core.Enums.PaymentStatus.rejected:
                                status = "odrzucona";
                                break;
                            case Core.Enums.PaymentStatus.processing:
                                status = "przetwarzana";
                                break;
                        }
                        SendEmail(user, "Aktualizacja statusu płatności", $"Status płatności za zamówienie o numerze: {order.Id} został zmieniony na {status}. W razie jakichkolwiek problemów proszę skontaktować się z administratorem.");
                    }

                    return HttpStatusCode.OK;
                }
                return HttpStatusCode.Forbidden;
            }
            return HttpStatusCode.MethodNotAllowed;
        }

        private bool checkSignature(PaymentUpdateDTO item, string PIN)
        {
            String inputString =
                PIN +
                item.Id.ToString() +
                item.Operation_number +
                item.Operation_type +
                item.Operation_status +
                item.Operation_amount +
                item.Operation_currency +
                item.Operation_original_amount +
                item.Operation_original_currency +
                item.Operation_datetime +
                item.Control +
                item.Description +
                item.Email +
                item.P_info +
                item.P_email +
                item.Channel.ToString();

            SHA256 sha256 = SHA256Managed.Create();
            byte[] bytes = Encoding.UTF8.GetBytes(inputString);
            byte[] hash = sha256.ComputeHash(bytes);
            String hashString = GetStringFromHash(hash);

            if (hashString.Equals(item.Signature.Trim(), StringComparison.InvariantCultureIgnoreCase))
                return true;
            else
                return false;
        }
        private string GetStringFromHash(byte[] hash)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                result.Append(hash[i].ToString("x2"));
            }
            return result.ToString();
        }

        public void HandleExpiredPayments()
        {
            var date = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.SpecifyKind(DateTime.UtcNow, DateTimeKind.Utc), "Central Europe Standard Time");
            var payments = UOW.Payments.GetAll().Where(x => (x.Operation_status != Core.Enums.PaymentStatus.completed && x.Operation_status != Core.Enums.PaymentStatus.rejected) && x.Expiration_time < date).ToList();
            var userIds = payments.Select(x => x.UserId).ToList();
            var users = UOW.Users.GetAll().Where(x => userIds.Contains(x.Id)).ToList();
            foreach (var item in payments)
            {
                var user = users.FirstOrDefault(x => x.Id == item.UserId);
                UOW.Payments.Delete(item.Id);
                UOW.Payments.Save();
                SendEmail(user, "Aktualizacja statusu płatności", $"Status płatności za zamówienie o numerze: {item.Control} został zmieniony na przedawniona. W razie jakichkolwiek problemów proszę skontaktować się z administratorem.");
            }
        }

        public FileDTO GetOrderInvoice(Guid id)
        {
            var userId = GetCurrentUserId();
            var currentUser = UOW.Users.GetById(userId);
            var order = UOW.Orders.GetIncludesById(id);
            if (order.Status != Core.Enums.OrderStatus.Zrealizowane)
                throw new CustomException("Brak danych", "Zamówienie nie zostało zrealizowane. Generowanie faktur możliwe jest jedynie dla zamówień, które zostały zrealizowane.");

            if (currentUser.Type == Core.Enums.UserType.client)
            {

                if (order.UserId != userId)
                    throw new CustomException("Brak dostępu", "Nie masz uprawnień do wykonania tej akcji");
            }
            else
                currentUser = UOW.Users.GetById(order.UserId);

            var configs = UOW.Configs.GetAll();
            var payment = UOW.Payments.GetByOrderId(id);
            var companyData = new Company()
            {
                Address = configs.FirstOrDefault(x => x.Name == "FVAddress").Value,
                Name = configs.FirstOrDefault(x => x.Name == "FVName").Value,
                Phone = configs.FirstOrDefault(x => x.Name == "FVPhone").Value,
                NIP = configs.FirstOrDefault(x => x.Name == "FVNIP").Value
            };
            var products = order.Products.Select(x => _mapper.Map<InvoiceProduct>(x)).ToList();

            var sb = new StringBuilder();
            sb.Append($"<!DOCTYPE html><html><head ><meta name=\"viewport\" content =\"width=device-width\" />    <title>Faktura_nr_{order.Id}</title></head>" +
                $"<body>" +
                    $"<div class='invoice-title'>" +
                        $"<span>Faktura nr: {order.Id}</span>" +
                        $"<span class='span'>Data wystawienia: {payment.Operation_datetime.ToString("yyy-MM-dd")}</span>" + 
                    "</div>" +
                $"<div class=\"invoice-head clearfix\">" +
                $"<div class=\"invoice-to\">Sprzedający:<br/>" +
                $"<strong>{companyData.Name}</strong><br/>{companyData.Address}<br/><abbr title='Telefon kontaktowy'>Tel:</abbr>{ companyData.Phone}</div>" +
                @"        <div class='invoice-details'>
            Kupujący:<br/>           
            Imię i nazwisko: " + currentUser.Firstname + " " + currentUser.Surname + @" <br/>
            Adres: " + order.Location + @"<br/>
            Telefon kontaktowy: " + currentUser.PhoneNumber + @"
        </div>
    </div>
<table>
        <tr>
            <th>
                Nazwa Produktu
            </th>
            <th>
                Cena jednostkowa
            </th>
            <th>
                Ilość
            </th>
        </tr> ");
            foreach (var item in products)
            {
                sb.Append(@"<tr>
                <td>
                    " + item.Name + @"
                </td>
                <td>
                    " + item.Price + @"
                </td>
                <td>
                    " + item.Quantity + @"
                </td>
            </tr>");
            }
            sb.Append(@"        <tr>
            <td colspan='1'></td>
            <td><strong>Suma:</strong></td>
            <td><strong>" + order.Cost + @"</strong></td>
        </tr>
    </table>

</body>
</html>");
            var globalSettings = new GlobalSettings
            {
                ColorMode = ColorMode.Color,
                Orientation = Orientation.Portrait,
                PaperSize = PaperKind.A4,
                Margins = new MarginSettings { Top = 10 },
                DocumentTitle = $"Faktura-{order.Id}"                
            };
            var objectSettings = new ObjectSettings
            {
                PagesCount = true,
                HtmlContent = sb.ToString(),
                WebSettings = { DefaultEncoding = "utf-8", UserStyleSheet = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "css" ,"invoice.css") },
                HeaderSettings = { FontName = "Arial", FontSize = 9, Right = "Strona [page] z [toPage]", Line = true }
            };
            var pdf = new HtmlToPdfDocument()
            {
                GlobalSettings = globalSettings,
                Objects = { objectSettings }
            };
            var file = _converter.Convert(pdf);
            return new FileDTO() { File = file, Name = $"Faktura-{order.Id}.pdf" };
        }
    }
}
