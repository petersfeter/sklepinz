﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SklepInternetowy.BL.Models
{
    public class ResultPage<TEntity> where TEntity : class
    {
        public List<TEntity> ListEntities { get; set; }
        public int TotalCount { get; set; }
    }
}
